/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   entry.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/19 02:01:47 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/01/24 15:04:12 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_ls.h"

/*
** fk_ls_entry_get_group
**
** Get group info. (The Norm doesn't allow static function prototyping...)
*/
static int	fk_ls_entry_get_group(t_ls_entry *self)
{
	t_ls_ginfo	*info;

	if (!(info = ft_ls_ginfo_new_for_entry(self)))
		return (FALSE);
	self->ginfo = info;
	return (TRUE);
}

t_ls_entry	*ft_ls_entry_new(const char *path)
{
	t_ls_entry	*self;

	self = ft_memalloc(sizeof(*self));
	self->path = ft_string_new(path);
	self->name = ft_string_new(ft_basename(path));
	return (self);
}

void		ft_ls_entry_destroy(t_ls_entry **self)
{
	if ((*self)->name)
		ft_string_destroy(&(*self)->name);
	if ((*self)->path)
		ft_string_destroy(&(*self)->path);
	if ((*self)->ginfo)
		ft_ls_ginfo_destroy(&(*self)->ginfo);
	free(*self);
	*self = NULL;
}

/*
** From https://linux.die.net/man/2/lstat
**
** struct stat {
**     dev_t     st_dev;     // ID of device containing file
**     ino_t     st_ino;     // inode number
**     mode_t    st_mode;    // protection
**     nlink_t   st_nlink;   // number of hard links
**     uid_t     st_uid;     // user ID of owner
**     gid_t     st_gid;     // group ID of owner
**     dev_t     st_rdev;    // device ID (if special file)
**     off_t     st_size;    // total size, in bytes
**     blksize_t st_blksize; // blocksize for file system I/O
**     blkcnt_t  st_blocks;  // number of 512B blocks allocated
**     time_t    st_atime;   // time of last access
**     time_t    st_mtime;   // time of last modification
**     time_t    st_ctime;   // time of last status change
** };
**
** If I ever implement different time flags the only difference will be from
** which statbuff I get the time (modified, accessed, changed status).
*/

/*
** ft_ls_entry_init
**
** Initialize all the info we need on an entry (apart from name and path which)
** were set on ft_ls_entry_new. On error errno will be set. Use get_entry_error
** to obtain a string for the error.
**
** @param {t_ls_entry *} self the entry to initialize
** @param {t_ls_info *} info the t_ls_info context to be used
** @returns {int} FALSE (0) on error and TRUE (1) on success
*/

int			ft_ls_entry_init(t_ls_entry *self, t_ls_info *info)
{
	int		status;

	if ((status = lstat(self->path->data, &self->statbuff)) != 0)
		return (FALSE);
	if ((info->flags & LS_FLAG_l) && !fk_ls_entry_get_group(self))
		return (FALSE);
	self->type = (S_ISREG(self->statbuff.st_mode)) ? LS_TYPE_REG : 0;
	self->type = (S_ISLNK(self->statbuff.st_mode)) ? LS_TYPE_LNK : self->type;
	self->type = (S_ISDIR(self->statbuff.st_mode)) ? LS_TYPE_DIR : self->type;
	return (TRUE);
}
