/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/26 17:42:31 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/01/22 15:28:04 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void
	fk_entry_lst_dst_func(void *ptr)
{
	ft_ls_entry_destroy((t_ls_entry **)&ptr);
}

int	ft_ls(t_ls_info *info)
{
	t_ls_entry	*entry;
	char		*fmt;

	while (info->queue)
	{
		entry = info->queue->content;
		if (ft_ls_entry_init(entry, info))
		{
			fmt = (entry->type == LS_TYPE_DIR) ? "{blue}%s{eoc}\n" : "%s\n";
			ft_printf(fmt, entry->name->data);
		}
		else
		{
			ft_dprintf(2, "ft_ls: '%s': %s\n",
						entry->path->data, get_entry_error());
		}
		ft_list_delete_with_func(&info->queue, 0, &fk_entry_lst_dst_func);
	}
	return (0);
}
