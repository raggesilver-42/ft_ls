/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/26 18:31:17 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/01/22 15:00:59 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include "libft.h"
# include "array/ft_array.h"
# include "ft_printf/ft_printf.h"
# include "io/util.h"
# include <time.h>
# include <dirent.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <unistd.h>
# include <dirent.h>
# include <errno.h>
# include <stdarg.h>
# include <grp.h>
# include <pwd.h>

/*
** Macros ======================================================================
*/

# define IS_DIR ft_ls_is_dir
# define IS_REG ft_ls_is_reg
# define IS_LNK ft_ls_is_lnk

/*
** Enums =======================================================================
*/

/*
** ls flags:
**
** -a include files/dirs that start with .
** -G disable color (bonus)
** -l list mode (perm, no hard links, owner, group, size, last modif, fname)
** -r reverse sort order
** -R recursive
** -t sort by last modified (recent first)
*/

enum				e_ls_flags
{
	LS_FLAG_a = 1 << 0,
	LS_FLAG_G = 1 << 1,
	LS_FLAG_l = 1 << 2,
	LS_FLAG_r = 1 << 3,
	LS_FLAG_R = 1 << 4,
	LS_FLAG_t = 1 << 5
};

enum				e_ls_types
{
	LS_TYPE_REG = 1 << 0,
	LS_TYPE_DIR = 1 << 1,
	LS_TYPE_LNK = 1 << 2
};

/*
** Types =======================================================================
*/

typedef struct		s_ls_ginfo
{
	t_string		*gname;
	t_string		*owner;
}					t_ls_ginfo;

typedef struct		s_ls_entry
{
	t_string		*name;
	t_string		*path;
	char			*error;
	int				perm;
	int				type;
	struct stat		statbuff;
	t_ls_ginfo		*ginfo;
}					t_ls_entry;

typedef struct		s_ls_error
{
	int				code;
	char			*msg;
}					t_ls_error;

typedef struct		s_ls_info
{
	const char		**argv;
	int				argc;
	t_uint8			flags;
	int				first : 1;
	t_list			*queue;
}					t_ls_info;

typedef struct		s_ls_level
{
	t_array			*files;
	t_array			*folders;
}					t_ls_level;

/*
** Global constants ============================================================
*/

extern const char			*g_flags;
extern const t_ls_error		*g_errors;

/*
** Functions ===================================================================
*/

void				die(int status, int show_usage,
					const char *fmt, ...) __attribute__((format(printf,3,4)));

void				ft_ls_parse_flags(t_ls_info *info);
char				*get_entry_error(void);

t_ls_entry			*ft_ls_entry_new(const char *path);
void				ft_ls_entry_destroy(t_ls_entry **self);
int					ft_ls_entry_init(t_ls_entry *self, t_ls_info *info);

t_ls_ginfo			*ft_ls_ginfo_new_for_entry(t_ls_entry *entry);
void				ft_ls_ginfo_destroy(t_ls_ginfo **self);

int					ft_ls_handle_file(t_ls_info *info, t_ls_entry *entry);

int					ft_ls(t_ls_info *info);

#endif
