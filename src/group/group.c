/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   group.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 02:16:45 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/01/22 14:17:51 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_ls.h"

/*
** From https://linux.die.net/man/3/getgrgid
**
** struct group {
**     char   *gr_name;       // group name
**     char   *gr_passwd;     // group password
**     gid_t   gr_gid;        // group ID
**     char  **gr_mem;        // group members
** };
**
** From https://linux.die.net/man/3/getpwuid
**
** struct passwd {
**     char   *pw_name;       // username
**     char   *pw_passwd;     // user password
**     uid_t   pw_uid;        // user ID
**     gid_t   pw_gid;        // group ID
**     char   *pw_gecos;      // user information
**     char   *pw_dir;        // home directory
**     char   *pw_shell;      // shell program
** };
*/

/*
** ft_ls_ginfo_new_for_entry
**
** Get group info for a given entry. Returns NULL if an error ocurred and errno
** will be set.
** @param {t_ls_entry *} entry the entry to get the group info for
** @returns {t_ls_ginfo *} the new group info for the entry or NULL.
*/

t_ls_ginfo	*ft_ls_ginfo_new_for_entry(t_ls_entry *entry)
{
	t_ls_ginfo		*self;
	struct group	*gp;
	struct passwd	*pw;

	if (!(gp = getgrgid(entry->statbuff.st_gid)))
		return (NULL);
	if (!(pw = getpwuid(entry->statbuff.st_uid)))
		return (NULL);
	self = malloc(sizeof(*self));
	self->gname = ft_string_new(gp->gr_name);
	self->owner = ft_string_new(pw->pw_name);
	return (self);
}

void		ft_ls_ginfo_destroy(t_ls_ginfo **self)
{
	if ((*self)->gname)
		ft_string_destroy(&(*self)->gname);
	if ((*self)->owner)
		ft_string_destroy(&(*self)->owner);
	free(*self);
	*self = NULL;
}
