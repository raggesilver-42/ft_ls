/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/22 14:45:07 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/01/22 14:59:26 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_ls.h"

/*
** This file contains functions used to handle file entries.
*/

int		ft_ls_handle_file(t_ls_info *info, t_ls_entry *entry)
{
	if (!ft_ls_entry_init(entry, info))
	{
		ft_dprintf(2, "ft_ls: '%s': %s\n",
					entry->path->data, get_entry_error());
	}
	return (TRUE);
}
