/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/26 17:42:31 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/01/22 15:06:51 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "ft_printf/ft_printf.h"
#include "io/util.h"

const char	*g_flags = "aGlrRt";

t_ls_info	*info_get_default(void)
{
	static t_ls_info info = { NULL, 0, 0, 0, NULL };

	return (&info);
}

static void	fk_entry_lst_dst_func(void *ptr)
{
	ft_ls_entry_destroy((t_ls_entry **)&ptr);
}

void __attribute__((destructor))
			fk_free_lst(void)
{
	t_ls_info *info;

	info = info_get_default();
	if (info->queue)
	{
		ft_list_destroy_with_func(&info->queue, fk_entry_lst_dst_func);
		ft_printf("[GC] cleared list\n");
	}
}

int			main(int argc, const char **argv)
{
	t_ls_info *info;

	info = info_get_default();
	info->argc = --argc;
	info->argv = ++argv;
	ft_ls_parse_flags(info);
	if (info->argc == 0)
		ft_list_append(&info->queue, ft_ls_entry_new("."));
	else
		while (*info->argv)
		{
			ft_list_append(&info->queue, ft_ls_entry_new(*info->argv++));
			--info->argc;
		}
	return (ft_ls(info));
}
