/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/18 22:20:39 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/01/22 14:19:41 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** This file contains the functions that handle command line parsing for ft_ls.
*/

#include "../ft_ls.h"

/*
** Parse flags from the command line. This also supports the flag '--' which
** indicates no more flag parsing needs to be done (essential if you have a file
** named '-lf', for example).
**
** Unlike it's first version this function does not handle any type of args,
** that is, it does not touch/handle the files and folders passed in to the
** program.
*/

void	ft_ls_parse_flags(t_ls_info *info)
{
	char	*aux;

	while (*info->argv)
	{
		if (ft_strequ(*info->argv, "--"))
			break ;
		if (**info->argv == '-')
			while (++(*info->argv) && **info->argv)
			{
				if ((aux = ft_strchr(g_flags, **info->argv)))
					info->flags |= (1 << (aux - g_flags));
				else
					die(1, TRUE, "ft_ls: illegal option -- %c\n", **info->argv);
			}
		else
			break ;
		++(*info->argv);
		--(info->argc);
	}
}
