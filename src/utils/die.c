/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   die.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/18 21:49:12 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/01/18 22:18:52 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_ls.h"

/*
** Display an error message and exit the program with status `status`.
**
** @param {int} status the status to exit with
** @param {int} show_usage whether to display usage message (1) or not (0)
** @param {const char *} fmt ft_printf formatted error str
** @param {...} ... arguments for ft_printf
** @noreturn
*/

void __attribute__((noreturn))
	die(int status, int show_usage, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	ft_vdprintf(2, fmt, ap);
	va_end(ap);
	if (show_usage)
		ft_dprintf((status == 0) ? 1 : 2, "usage: ft_ls [-%s] [file ...]\n",
			g_flags);
	exit(status);
}
