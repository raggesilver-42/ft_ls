/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/18 22:12:08 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/01/18 22:14:51 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_ls.h"

const t_ls_error	*g_errors = (t_ls_error[]){
	{ EACCES, "Permission denied" },
	{ ENOENT, "No such file or directory" },
	{ -1, "Error" },
};

/*
** Get a str error from errno.
**
** @returns {char *} error str (weak)
*/

char	*get_entry_error(void)
{
	t_ls_error	*e;

	e = (t_ls_error *)g_errors;
	while ((*e).code != -1)
	{
		if (errno == (*e).code)
			return ((*e).msg);
		e++;
	}
	return ((*e).msg);
}
