#!/bin/bash

vg=

# Disable valgrind for OSX (cuz it sucks)
if [[ $(uname) != "Darwin" ]]; then
	vg="valgrind --leak-check=full --track-origins=yes --show-leak-kinds=all --error-exitcode=1"
fi

$vg ../ft_ls || exit $?
